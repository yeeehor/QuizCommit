import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  arrRoutes = [
    {link: '/quiz', name: 'Quiz'},
    {link: '/quiz-editor', name: "Quiz Editor"},
    {link: '/memory-card', name: "Memory Card"},
    {link: '/about', name: 'About'}
];
  constructor() { }

  ngOnInit() {
  }

}
