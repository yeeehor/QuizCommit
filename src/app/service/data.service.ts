import { Injectable, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import {QuizModel} from '../model/quiz.model';
import { LocalStorageService } from './localStorage.service';

@Injectable()
export class DataService {
  url = './assets/questionData.json';
  loading$ = new Subject<boolean>();

  constructor(private http: HttpClient, private localStorage: LocalStorageService) {
  }
  // getQuestions(): Observable<QuizModel[]> {
  //     this.loading$.next(true);
  //     return this.http.get<QuizModel[]>(this.url).delay(500).do(() => {
  //       this.loading$.next(false);
  //     });
  //   }

  receiveQuestions() {
    this.loading$.next(true);
      return Observable.of(this.localStorage.getQuestions()).delay(300).do(() => {
        this.loading$.next(false);
      });
  }

  setQuestions(questions: QuizModel[]) {
    this.localStorage.saveQuestions(questions);
  }

}

