import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { QuizModel } from './../model/quiz.model';

@Injectable()
export class LocalStorageService {

    getQuestions(){
        return JSON.parse(window.localStorage.getItem('questions'));
    }
    saveQuestions(questions: QuizModel[]) {
        window.localStorage.setItem('questions', JSON.stringify(questions));
    }
}