import {QuizModel} from './../model/quiz.model';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, FormControl, Validators, FormArray} from '@angular/forms';

@Component({
  selector: 'app-quiz-dialog',
  templateUrl: './quiz-dialog.component.html',
  styleUrls: ['./quiz-dialog.component.css']
})
export class QuizDialogComponent implements OnInit {
  form: FormGroup;
  constructor( private dialogRef: MatDialogRef<QuizDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data) {
  }

  ngOnInit() {
    this.createForm(this.data);
  }

  createForm(data) {
    this.form = new FormGroup({
      'text': new FormControl(data.text || '', Validators.required),
      'correctAnswer': new FormControl(data.correctAnswer || 0, Validators.required),
      'answers': new FormArray([])
    });
    if (!this.data.answers.length) {
      this.form.setControl('answers', new FormArray([new FormControl('', Validators.required), new FormControl('', Validators.required)]));
    } else {
      let arrayAnswers = new FormArray([]);
      this.data.answers.forEach((answer) => {
        arrayAnswers.push(new FormControl(answer, Validators.required));
      });
      this.form.setControl('answers', arrayAnswers);
    }
  }

  get answersArray(): FormArray {
    return <FormArray>this.form.get('answers');
  }

  addAnswer() {
    const control = new FormControl(null, Validators.required);
    (<FormArray>this.form.get('answers')).push(control);
  }

  deleteAnswer(index: number) {
    this.answersArray.removeAt(index);
  }

  onSubmit() {
    this.dialogRef.close(this.form.value);
  }

  close() {
    this.dialogRef.close();
  }

}
