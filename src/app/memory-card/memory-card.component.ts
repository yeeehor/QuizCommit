import {Component, OnInit, OnChanges} from '@angular/core';
import {Card} from '../model/card.model';

@Component({
  selector: 'app-memory-card',
  templateUrl: './memory-card.component.html',
  styleUrls: ['./memory-card.component.css']
})
export class MemoryCardComponent {
  cards: Card[];
  selectedLevel: number;
  moves: number;
  total: number = 0;
  gameStarted: boolean;
  pairs: number;
  randomArray = [];
  random = [];
  max: number = 30;
  card1 = -1;
  card2 = -1;
  cardBuffer: Card[];
  levels = [
    {id: 4, name: 'Low'},
    {id: 8, name: 'Medium'},
    {id: 16, name: 'Hard'}
  ];

  constructor() {
  }

  startNewGame() {
    this.moves = 0;
    this.total = 0;
    this.randomArray = [];
    this.generateRandomCards();
    this.createCard();
  }

  showCard(index: number) {
    if(this.card1 === -1){
      this.card1 = index; 
      this.cards[this.card1].isFlipped = true;
    } 
    else if(this.card2 === -1){
      this.card2 = index;
      this.cards[this.card2].isFlipped = true;
      this.moves++;
      this.total = Math.round((this.selectedLevel/this.moves)*100);
      if(this.cards[this.card1].id !== this.cards[this.card2].id){
        setTimeout(() => {
          this.cards[this.card1].isFlipped = false;
          this.cards[this.card2].isFlipped = false;
          this.card1 = -1;
          this.card2 = -1;
        }, 500)
        
      } else{
          this.cards[this.card1].isFlipped = true;
          this.cards[this.card2].isFlipped = true;
          this.card1 = -1;
          this.card2 = -1;
          
      }
    }
 }

  createCard() {
    this.cards = [];
    this.randomArray.forEach((num) => {
      this.cards.push({id: num, isFlipped: false});
    });
    console.log(this.cards);
  }

  generateRandomCards() {
    let random = [];
    for (let i = 0; i < this.selectedLevel; i++) {
      let temp = Math.floor(Math.random() * this.max);
      if (random.indexOf(temp) == -1) {
        random.push(temp);
      } else {
        i--;
      }
    }
    this.randomArray.push(...random);
    this.randomArray.push(...this.shuffleArray(random));
  }

  shuffleArray(array) {
    let currentIndex = array.length;
    let temporaryValue;
    let randomIndex;
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }
}
