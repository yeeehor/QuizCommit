import { Observable } from 'rxjs/Observable';
import {Component, OnInit} from '@angular/core';
import {DataService} from '../service/data.service';
import {QuizModel} from '../model/quiz.model';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {
  questions: QuizModel[] = [];
  total = 0;
  sum = 0;
  completed = false;
  indexQuestion = 0;

  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.dataService.receiveQuestions().subscribe((question) => {
      this.questions = question;
    });
  }

  onPrevious() {
    this.indexQuestion--;
  }

  onNext() {
    this.indexQuestion++;
  }

  onSelectItem(indexQuestion, answerIndex) {
    this.questions[indexQuestion].selected = answerIndex;
  }

  showResult() {
    this.total = 0;
    this.completed = true;
    const markAnswer = 100 / this.questions.length;
    this.questions.forEach((question) => {
      if (question.correctAnswer === question.selected) {
        this.sum = Math.round(this.total += markAnswer);
      }
    });
  }
}
