
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import { AppComponent } from './app.component';
import { QuizComponent } from './quiz/quiz.component';
import {DataService} from './service/data.service';
import {HttpClientModule} from '@angular/common/http';
import {MatButtonModule,
        MatCardModule,
        MatCheckboxModule,
        MatRadioModule,
        MatTabsModule,
        MatToolbarModule,
        MatIconModule,
        MatDialogModule,
        MatInputModule} from '@angular/material';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoadProgressComponent} from './load-progress/load-progress.component';
import { HeaderComponent } from './header/header.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AboutComponent } from './about/about.component'
import { QuizEditorComponent } from './quiz-editor/quiz-editor.component';
import { QuizDialogComponent } from './quiz-dialog/quiz-dialog.component';
import { LocalStorageService } from './service/localStorage.service';
import { MemoryCardComponent } from './memory-card/memory-card.component';

const appRoutes: Routes = [
  {path: 'quiz', component: QuizComponent},
  {path: 'quiz-editor', component: QuizEditorComponent},
  {path: 'about', component: AboutComponent},
  {path: 'memory-card', component: MemoryCardComponent}
  ];
@NgModule({
  declarations: [
    AppComponent,
    QuizComponent,
    LoadProgressComponent,
    HeaderComponent,
    QuizEditorComponent,
    AboutComponent,
    QuizDialogComponent,
    MemoryCardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatButtonModule,
    MatCheckboxModule,
    MatRadioModule,
    MatProgressBarModule,
    MatTabsModule,
    MatCardModule,
    MatToolbarModule,
    MatInputModule,
    MatIconModule,
    MatDialogModule,
    MatRadioModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes)
  ],
  entryComponents: [QuizDialogComponent],
  providers: [DataService, LocalStorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
