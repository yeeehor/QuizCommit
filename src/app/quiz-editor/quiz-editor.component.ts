import {DataService} from './../service/data.service';
import {QuizModel} from './../model/quiz.model';
import {QuizDialogComponent} from './../quiz-dialog/quiz-dialog.component';
import { Component, Inject, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';


@Component({
  selector: 'app-quiz-editor',
  templateUrl: './quiz-editor.component.html',
  styleUrls: ['./quiz-editor.component.css']
})
export class QuizEditorComponent implements OnInit {
  quizDialogRef: MatDialogRef<QuizDialogComponent>;
  questions: QuizModel[] = [];
  array: QuizModel[] = [];

  constructor(private dialog: MatDialog, private dataService: DataService) {
  }

  ngOnInit(){
    this.getAllQuestions();
  }

  getAllQuestions(){
    // this.questions.push(...this.dataService.receiveQuestions());
    this.dataService.receiveQuestions().subscribe((question) => {
      this.questions.push(...question);
    });
  }

  openAddQuestion() {
    this.quizDialogRef = this.dialog.open(QuizDialogComponent, {
      hasBackdrop: false,
      width: '600px',
      data: new QuizModel()
    });
    this.quizDialogRef.afterClosed().subscribe((question: QuizModel) => {
      if (question) {
        this.questions.push(question);
        this.pushQuestion();
      }
    });
  }

  openEditQuestion(question: QuizModel, index: number){
    this.quizDialogRef = this.dialog.open(QuizDialogComponent, {
      hasBackdrop: false,
      width: '600px',
      data: question
    });
    this.quizDialogRef.afterClosed().subscribe((question: QuizModel) => {
      if(question){
        this.questions[index] = question;
        this.pushQuestion();
      }
    })
  }

  pushQuestion() {
    this.dataService.setQuestions(this.questions);
  }

  deleteQuestion(index) {
    this.questions.splice(index, 1);
    this.pushQuestion();
  }

}
